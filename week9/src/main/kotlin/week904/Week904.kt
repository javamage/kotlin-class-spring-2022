package week904

class MyLinkedList<TYPE> {
    class Node<TYPE>(
        val value: TYPE,
        var next: Node<TYPE>? = null
    )

    private var head: Node<TYPE>? = null
    var size: Int = 0
        private set

    fun insert(value: TYPE) {
        head = Node(value, head)
        size++
    }

    operator fun get(index: Int): TYPE {
        var temp = head
        repeat(index) {
            temp = temp?.next
        }
        return temp?.value ?: throw IndexOutOfBoundsException()
    }
}


fun copy(from: MyLinkedList<Int>, to: MyLinkedList<Int>) {
    (0 until from.size).forEach {
        to.insert(from[it])
    }
}

fun copy2(from: MyLinkedList<Mammal>, to: MyLinkedList<Mammal>) {
    (0 until from.size).forEach {
        to.insert(from[it])
    }
}

fun <ITEM_TYPE> copy3(from: MyLinkedList<ITEM_TYPE>, to: MyLinkedList<ITEM_TYPE>) {
    (0 until from.size).forEach {
        to.insert(from[it])
    }
}

abstract class Mammal {
    abstract val name: String
}
open class Dog(override val name: String): Mammal()
open class Cat(override val name: String): Mammal()
open class Tabby(name: String): Cat(name)

fun main() {
    val list1 = MyLinkedList<Int>().apply {
        insert(5)
        insert(4)
        insert(3)
        insert(2)
        insert(1)
    }
    val list2 = MyLinkedList<Int>()
    copy(from = list1, to = list2)
    (0 until list2.size).forEach {
        println(list2[it])
    }

    val list3 = MyLinkedList<Mammal>().apply {
        insert(Dog("Fido"))
        insert(Cat("Fluffy"))
        insert(Tabby("Thunderbean"))
        insert(Dog("Rex"))
        insert(Cat("Puffball"))
    }
    (0 until list3.size).forEach {
        println(list3[it].name)
    }
    val list4 = MyLinkedList<Mammal>()
    copy2(from = list3, to = list4)
    (0 until list4.size).forEach {
        println(list4[it].name)
    }
    val list5 = MyLinkedList<Cat>().apply {
        insert(Cat("Fluffy"))
        insert(Tabby("Thunderbean"))
        insert(Cat("Puffball"))
    }
    val list6 = MyLinkedList<Cat>()
    copy3(from = list1, to = list2)
    copy3(from = list3, to = list4)
    copy3(from = list5, to = list6)
    (0 until list6.size).forEach {
        println(list6[it].name)
    }
//    copy3(from = list5, to = list3)
}