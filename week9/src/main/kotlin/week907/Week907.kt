package week907

import week902.MyLinkedList
import kotlin.reflect.KClass

fun foo() {
    val list = MyLinkedList<String>()

}

fun printSize(list: MyLinkedList<*>) {
    println(list.size)
}

fun interface Processor<T:Any> {
    fun process(value: T)
}


private val processors = mutableMapOf<KClass<*>, (Any) -> Unit>()
private val processors2 = mutableMapOf<KClass<*>, Processor<*>>()

fun register1(kClass: KClass<*>, processor: (Any) -> Unit) {
    processors[kClass] = processor
}

fun <T: Any> register2(kClass: KClass<T>, processor: Processor<T>) {
    processors2[kClass] = processor
}

inline fun <reified T: Any> register2a(processor: Processor<T>) =
    register2(T::class, processor)

fun <T: Any> register3(kClass: KClass<T>, processor: (T) -> Unit) {
    processors[kClass] = processor as (Any) -> Unit
}

fun process(value: Any) {
    processors[value::class]?.invoke(value) ?: throw IllegalStateException("No processor registered for type ${value::class.simpleName}")
}
fun <T:Any> process2(value: T) {
    (processors2[value::class] as? Processor<T>)?.process(value) ?: throw IllegalStateException("No processor registered for type ${value::class.simpleName}")
}

open class View
fun <T: View> findViewById(id: Int): T {
    return View() as T
}

fun main() {
    val view1 = findViewById(42) as View
    val view2 = findViewById<View>(42)

    register2(String::class) {
        println(it.length)
    }
    register2a { s: String ->
        println(s.length)
    }
    register2a<String> {
        println(it.length)
    }
    register3(String::class) {
        println(it.length)
    }
    process("Scott")
    process2("Scott")
}