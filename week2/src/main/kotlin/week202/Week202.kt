package week202

fun main() {
    println(createStringOfNumbers3(numberOfThingsToPrint = 4))
    println(factorial2(3))

    val xxx = createStringOfNumbers2Bad(10)
    println(xxx)

    draw10x10Box(
        drawBorder = true,
        x = 10, y = 10,
        borderWidth = 2,
        fillBackground = true
    )
    draw10x10Box(
        x = 10, y = 10,
    )
}

// Java made you do this...
fun multiParamFunction(x: Int) {
    multiParamFunction(x, 0)
}
fun multiParamFunction(x: Int, y: Int) {
    multiParamFunction(x, y, 0)
}
fun multiParamFunction(x: Int, y: Int, z: Int) {}







fun ternary1(x: Int) {
    println(if (x < 100) "less" else "more")
}

fun createStringOfNumbers2(numberOfThingsToPrint: Int): String {
    return if (numberOfThingsToPrint == 1) {
        "1"
    } else {
        createStringOfNumbers2(numberOfThingsToPrint - 1) + " " + numberOfThingsToPrint
    }
}

fun createStringOfNumbers2a(numberOfThingsToPrint: Int) =
    if (numberOfThingsToPrint == 1) {
        "1"
    } else {
        createStringOfNumbers2(numberOfThingsToPrint - 1) + " " + numberOfThingsToPrint
    }

fun createStringOfNumbers2Bad(numberOfThingsToPrint: Int) =
    if (numberOfThingsToPrint == 1) {
        "1"
    } else {
        createStringOfNumbers2(numberOfThingsToPrint - 1) + " " + numberOfThingsToPrint
    }

tailrec fun createStringOfNumbers3(numberOfThingsToPrint: Int, answerSoFar: String = ""): String =
    if (numberOfThingsToPrint == 1) {
        "1 $answerSoFar".trim()
    } else {
        createStringOfNumbers3(
            numberOfThingsToPrint - 1,
            "$numberOfThingsToPrint $answerSoFar"
        )
    }

fun factorial(n: Int): Int =
    if (n == 0 || n == 1) {
        1
    } else {
        factorial(n-1) * n
    }

fun factorial2(n: Int) = factorial2Internal(n, 1)

tailrec fun factorial2Internal(n: Int, answerSoFar: Int): Int { // answerSoFar is an accumulator
    if (n == 0 || n == 1) {
        return answerSoFar
    }
    return factorial2Internal(n-1, answerSoFar * n)
}

fun getNumberOfThingsToPrint1(): Int {
    return 5
}

fun getNumberOfThingsToPrint2(): Int = 5

fun getNumberOfThingsToPrint3() = 7

// PUBLIC/FINAL BY DEFAULT
fun draw10x10Box(x: Int, y: Int, drawBorder: Boolean = true, borderWidth: Int = 2, fillBackground: Boolean = true) {
    // do some painting
}

open class Foo {
    @JvmOverloads
    fun draw10x10Box(x: Int, y: Int, drawBorder: Boolean = true, borderWidth: Int = 2, fillBackground: Boolean = true) {
        // do some painting
    }
}