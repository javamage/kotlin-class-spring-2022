package week201

fun main() {
    printStringOfNumbers(getNumberOfThingsToPrint3())
    println(createStringOfNumbers3(4))
    println(factorial2(3))
}


fun printStringOfNumbers(numberOfThingsToPrint: Int) { // default return type is Unit
    val numberString = createStringOfNumbers(numberOfThingsToPrint)
    println(numberString)
}

fun createStringOfNumbers(numberOfThingsToPrint: Int): String {
    var result = ""
    for(n in 1 .. numberOfThingsToPrint) {
        result = "$result$n "
    }
    return result
}

fun createStringOfNumbers2(numberOfThingsToPrint: Int): String {
    if (numberOfThingsToPrint == 1)
        return "1"
    else
        return createStringOfNumbers2(numberOfThingsToPrint-1) + " " + numberOfThingsToPrint
}

fun createStringOfNumbers3(numberOfThingsToPrint: Int) = createStringOfNumbers3Internal(numberOfThingsToPrint, "")
tailrec fun createStringOfNumbers3Internal(numberOfThingsToPrint: Int, answerSoFar: String): String {
    if (numberOfThingsToPrint == 1)
        return "1 $answerSoFar".trim()
    else
        return createStringOfNumbers3Internal(
            numberOfThingsToPrint-1,
            "$numberOfThingsToPrint $answerSoFar"
        )
}

fun factorial(n: Int): Int {
    if (n == 0 || n == 1) {
        return 1
    } else {
        return factorial(n-1) * n
    }
}

fun factorial2(n: Int) = factorial2Internal(n, 1)

tailrec fun factorial2Internal(n: Int, answerSoFar: Int): Int { // answerSoFar is an accumulator
    if (n == 0 || n == 1) {
        return answerSoFar
    }
    return factorial2Internal(n-1, answerSoFar * n)
}

fun getNumberOfThingsToPrint1(): Int {
    return 5
}

fun getNumberOfThingsToPrint2(): Int = 5

fun getNumberOfThingsToPrint3() = 7
