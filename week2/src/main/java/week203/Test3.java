package week203;

public class Test3 {
    public static void main(String[] args) {
        foo2("A", "B", "C");
        String[] strings = {"A", "B", "C"};
        foo2((String[]) strings);
    }

    public static void foo2(Object... objects) {
        for(Object object: objects) {
            System.out.println(object);
        }
    }

    public static void foo(String... strings) {
        for(String string: strings) {
            System.out.println(string);
        }
    }
}
