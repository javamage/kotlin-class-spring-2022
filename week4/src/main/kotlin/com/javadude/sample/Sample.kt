package com.javadude.sample
    // package names recommended to start with reversed domain name

// can alias to resolve conflicts
//    import com.javadude.sample.data.Person as ComplexPerson
//    import com.javadude.sample.data2.Person as SimplePerson

// really really really bad idea
//   "import on demand"
// see https://www.javadude.com/posts/20040522-import-on-demand-is-evil/
import com.javadude.sample.data.*

// alias examples
//val person = ComplexPerson(name = "Scott", age = 55)
//val person2 = SimplePerson("Scott")

val person3 = Person(name = "Mike", age = 34)
