package com.javadude.sample.data

class PersonDao {
    fun insert(person: Person) { TODO("put in database") }
    fun delete(person: Person) { TODO("delete from database") }
    fun update(person: Person) { TODO("update in database") }
    fun get(id: String): Person { TODO("find Person by id from database") }
}