package nullability;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

class Person {
    public String name; // BAD IDEA, BUT THIS IS A SILLY EXAMPLE ANYWAY
    public int age;
}

public class JavaNulls {
    private Person person; // is null a valid value?
        // in this case, it has to be - null is the default
        // in java - default initial values are 0, false or null

    private Person person2 = new Person(); // but here - can we assign null???

    public void setPerson2(Person person) {
        if (person == null) {
            throw new IllegalArgumentException("Cannot use null Person");
        }
        this.person2 = person;
    }

    @Nullable private Person person3 = new Person(); // explicitly allow nulls

    public void setPerson3(@Nullable Person person) {
        this.person3 = person;
    }

    @NotNull private Person person4 = new Person(); // explicitly disallow nulls

    public void setPerson4(@NotNull Person person) {
        this.person4 = person;
    }

    public void setPerson4Indirect(Person person) {
        setPerson4(person);
    }


    public static void main(String[] args) {
        JavaNulls javaNulls = new JavaNulls();
        javaNulls.setPerson4Indirect(null);
    }
}
