package org.example

fun main() {
    val company = company {
        name = "Muppets Inc"
        ceo {
            name = "Kermit"
            minions {
                manager {
                    name = "Piggy"
                    minions {
                        employee {
                            name = "Gonzo"
                        }
                        employee {
                            name = "Rowlf"
                        }
                    }
                }
                manager {
                    name = "Fozzie"
                    // he has no minions... they don't trust him
                }
                manager {
                    name = "Swedish Chef"
                    minions {
                        (1..5).forEach { n ->
                            employee {
                                name = "Chicken $n"
                            }
                        }
                    }
                }
                employee {
                    name = "Scooter"
                }
            }
        }
    }
    company.printStaff()
}


fun company(init: CompanyBuilder.() -> Unit): Company{
    val builder = CompanyBuilder()
    builder.init()
    return builder.build()
}

@DslMarker
annotation class Builder

@Builder
class CompanyBuilder {
    var name: String? = null
    private var ceo: Manager? = null
    fun ceo(init: ManagerBuilder.() -> Unit) {
        if (ceo != null) {
            throw IllegalStateException("CEO already exists!")
        }
        val builder = ManagerBuilder()
        builder.init()
        ceo = builder.build()
    }
    fun build() =
        name?.let { name ->
            ceo?.let { ceo ->
                Company(name, ceo)
            }
        } ?: throw IllegalStateException("name or ceo not set")
}

@Builder
open class ManagerBuilder {
    var name: String? = null
    private var minions: List<Employee> = emptyList()
    fun minions(init: MinionsBuilder.() -> Unit) {
        val builder = MinionsBuilder()
        builder.init()
        minions = builder.build()
    }
    fun build() =
        name?.let {
            Manager(it, minions)
        } ?: throw IllegalStateException("Name not set")
}

@Builder
open class MinionsBuilder {
    private val minions = mutableListOf<Employee>()
    fun manager(init: ManagerBuilder.() -> Unit) {
        val builder = ManagerBuilder()
        builder.init()
        minions.add(builder.build())
    }
    fun employee(init: EmployeeBuilder.() -> Unit) {
        val builder = EmployeeBuilder()
        builder.init()
        minions.add(builder.build())
    }
    fun build() = minions.toList()
}

@Builder
class EmployeeBuilder {
    var name: String? = null
    fun build() =
        name?.let {
            Employee(it)
        } ?: throw IllegalStateException("Name not set")
}

open class Employee(
    val name: String,
) {
    open fun printStaff(indent: String) {
        println("$indent$name")
    }
}

open class Manager(
    name: String,
    val minions: List<Employee>,
): Employee(name) {
    override fun printStaff(indent: String) {
        println("$indent$name")
        minions.forEach {
            it.printStaff("  $indent")
        }
    }
}

class Company(
    val name: String,
    val ceo: Employee,
) {
    fun printStaff() {
        ceo.printStaff("")
    }
}
