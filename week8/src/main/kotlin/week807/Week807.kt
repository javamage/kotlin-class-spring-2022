package week807

fun main() {
    val list = listOf("A", "B", "C", "D")

    list
        .map { "XXX${it}XXX" }
        .filter { "C" in it }
        .onEach { println(it) }

    list
        .asSequence()
        .map { "XXX${it}XXX" }
        .filter { "C" in it }
        .forEach { println(it) }
}