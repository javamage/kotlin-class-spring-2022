package week802

import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.FileWriter
import java.io.PrintWriter

// File I/O and exceptions

// File class - abstract file name

fun main() {
    val file = File("hello.txt")
    // write text - opens, writes and closes the file
    // takes a single string
    // should only use if "small-ish" limited input
    // this will OVERWRITE all data in the file
    file.writeText("""
        Line 1
        Line 2
        Line 3
        Line 4
    """.trimIndent())
    file.appendText("""
        Line 5
        Line 6
        Line 7
    """.trimIndent())

    val text = file.readText()
    println(text)


    // if we want to write and read lots of data, work in chunks

    // two types of file input/output
    //    binary - InputStream and OutputStream
    //    text - Reader and Writer

    // can use the Decorator and Adapter patterns to modify input and output
    //   these are wrappers

    // Coffee Pot = file
    // Coffee Filter = tweaks your data

    // THE FOLLOWING EXAMPLES ARE ALL EVIL!!!
    // IF EXCEPTION DURING WRITES, WE'LL SKIP THE CLOSE!!!
    val file1 = File("File1.txt")
    val fw1 = FileWriter(file1)
    (1..100).forEach {
        fw1.write(it.toString())
        fw1.write("\n")
    }
    fw1.close()

    val file2 = File("File2.txt")
    val fw2 = FileWriter(file2)
    val pw2 = PrintWriter(fw2)
    (1..100).forEach {
        pw2.println(it.toString())
    }
    pw2.close()

    val file3 = File("File3.bin")
    val fos3 = FileOutputStream(file3)
    (1..100).forEach {
        fos3.write(it)
    }
    fos3.close()

    val fis3 = FileInputStream(file3)
    (1..100).forEach { _ ->
        println(fis3.read())
    }
    fis3.close()
}