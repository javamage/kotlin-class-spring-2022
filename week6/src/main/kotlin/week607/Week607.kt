package week607

class TersePerson(
    var name: String = "",
    var age: Int = 0
)

class Person(name: String, age: Int) {
    var name: String = ""
    var age: Int = 0

    constructor(name: String): this(name, 0)

    init {
        this.name = name
        this.age = age
    }

    // java equivalent
    //   private String name = "";
    //   private int age = 0;
    //   public void setName(name: String) { this.name = name; }
    //   public void setAge(age: Int) { this.age = age; }
    //   public String getName() { return this.name; }
    //   public int getAge() { return this.age; }
    //   public Person(String name, int age) {
    //      this.name = name;
    //      this.age = age;
    //   }
}
