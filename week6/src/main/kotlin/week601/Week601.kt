package week601

// enum classes
enum class Suit(
    val fancyName: String
) {
    Spades("I am Spades!"),
    Hearts("I am Hearts!"),
    Diamonds("I am Diamonds!"),
    Clubs("I am Clubs!");
        // one of VERY few places in kotlin that needs a semicolon

    fun trumps(otherSuit: Suit) =
        ordinal < otherSuit.ordinal
}

data class Card(
    val rank: Int,
    val suit: Suit
)

fun main() {
    val card1 = Card(1, Suit.Clubs)
    val card2 = Card(1, Suit.Spades)

    println(card1.suit.trumps(card2.suit))
    println(card2.suit.trumps(card1.suit))

    val result = when (card1.suit) {
        Suit.Spades -> 1
        Suit.Hearts -> 2
        Suit.Diamonds -> 3
        Suit.Clubs -> 4
    }
    val result2 = when (card1.suit) {
        Suit.Spades, Suit.Hearts -> card1.suit.fancyName
        else -> "I am a lowly card"
    }
}