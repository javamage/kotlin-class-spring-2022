package week504

// objects and sealed classes/interfaces

// sealed class/interface -> subclasses can ONLY be defined in this module

sealed interface Response
object Loading: Response
object TimedOut: Response
data class Loaded(val value: String): Response
data class Error(val message: String, val exception: Throwable): Response

fun getValue(): String = ""

fun getNameFromWebService(id: String, onResponse: (Response) -> Unit) {
    // most likely - start a thread here (or better launch coroutine)
    onResponse(Loading)
    // get result from web service
    try {
        val value = getValue()
        onResponse(Loaded(value))
    } catch (e: Throwable) {
        onResponse(Error("Could not load $id", e))
    }
}

fun main() {
    getNameFromWebService("42") { response ->
        when (response) {
            Loading -> {} // put a spinning progress meter on the screen with word "loaded"
            TimedOut -> {} // stop progress meter and pop up "timed out" message
            is Loaded -> {} // stop progress meter and use the loaded data
            is Error -> {} // stop progress meter and report error
        }
    }
}

