package week508

import java.awt.event.ActionListener

typealias ActionListener2 = () -> Unit

fun interface ActionListener1 {
    fun actionPerformed()
}

fun button(
    actionListener: ActionListener
) {
    actionListener.actionPerformed(null)
}

fun button1(
    actionListener: ActionListener1
) {
    actionListener.actionPerformed()
}

fun button2(
    actionListener: ActionListener2
) {
    actionListener()
}

fun main() {
    button {
        println("a")
    }
    button1(ActionListener1 {
        print("a")
    })
    button2 {
        println("a")
    }
}