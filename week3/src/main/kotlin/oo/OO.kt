package oo

// Tenets of Object-Oriented Programming
//   * Encapsulation
//       - hiding implementation details
//       - protecting implementation details
//
//   * Inheritance
//       - create specializations of types to add or extend/replace behavior
//
//   * Polymorphism
//       - "many forms"
//       - overloading functions/methods
//          same name, different parameter types
//       - overriding functions/methods
//           in subclass, same signature, different behavior


open class A {
    open fun foo(n: Int) {
        println(n)
    }
}
class B: A() {
    // override function to change behavior
    override fun foo(n: Int) {
        super.foo(n + 42)
    }
}

class List {
    // overloading function names
    // name + parameter types = signature
    fun add(number: Int) {}
    fun add(position: Int, number: Int) {}
    fun add(vararg number: Int) {}
//    fun add(number: Int) {}
//    fun addAtPosition(position: Int, number: Int) {}
//    fun addMultiple(vararg number: Int) {}
}

var peanutButterAmount = 32

fun spreadPB(amount: Int) {
    peanutButterAmount -= amount
}

