package oo

data class Person42(
    val firstName: String,
    val lastName: String
) {
    val name1: String = "$firstName $lastName"
        // has backing field with value - only good if dependent properties are VALS

    val name2: String // no backing field
        get() = "$firstName $lastName"
        // good if dependent properties are VARS
}

// different implementation of derived property
//    update derived value whenever dependent properties change
//    this is good if the derived value is expensive to compute
class Person43(firstName: String, lastName: String) {
    var firstName: String = ""
        set(value) {
            field = value
            name = "$firstName $lastName"
        }
    var lastName: String = ""
        set(value) {
            field = value
            name = "$firstName $lastName"
        }
    var name: String = ""
        private set

    init {
        this.firstName = firstName
        this.lastName = lastName
    }
}


fun main() {
    val person = Person43("Scott", "Stanchfield")
    println(person.firstName)
    println(person.lastName)
    println(person.name)

    person.firstName = "Steve"
}