package oo

// class = blueprint
//    use this to create instances of a Person
open class Person {
    var name: String = ""
    var age: Int = 0
}

// BAD example - don't write like this
open class Person2(name: String, age: Int) { // primary constructor
    var name: String
    var age: Int

    init { // constructor body
        this.name = name
        this.age = age
    }
}

open class Person3(var name: String, var age: Int) // primary constructor defining properties
    // note - cannot override get/set

// NOTE - superclass of everything is Any
open class Person4(var name: String, var age: Int) {
    override fun toString(): String {
        return "Person4(name='$name', age=$age)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true // === means same instance
        if (javaClass != other?.javaClass) return false

        other as Person4

        if (name != other.name) return false
        if (age != other.age) return false

        return true
    }

    // used in HashMaps - if two instances are equals() - hashCode MUST be the same
    // HashMap
    //    buckets of data
    //    distribute data into bucket hashCode() % numberOfBuckets
    //    buckets typically have a list of items
    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + age
        return result
    }
}

data class Person5(var name: String, var age: Int)
    // defined for the parameters to the primary constructor
    //    toString()
    //    hashCode()
    //    equals()
    //    copy() with all parameters defaulted to current value
    // component1() for first parameter to constructor
    // component2() for second parameter to constructor
    // component3() for third parameter to constructor
    //    ...
    // PROBLEM - cannot create subclasses

data class Numbers(val n1: Int, val n2: Int)
fun generateNextTwoNumbers(n: Int) =
    Numbers(n+1, n+2)
fun generateNextTwoNumbers2(n: Int) =
    arrayOf(n+1, n+2)

// not a good design for a Location
open class Location private constructor(
    val lat : Double,
    val lon : Double,
    val street : String,
    val city : String,
    val state : String,
    val zip : String
) {
    constructor(lat: Double, lon: Double): this(lat, lon, "", "", "","")
    constructor(
        street : String,
        city : String,
        state : String,
        zip : String
    ): this(0.0, 0.0, street, city, state, zip)
}

// calling superclass constructor
abstract class Mammal(val name: String)
open class Cat(name: String) : Mammal(name)

// abstract class - cannot create instances
//    if class has abstract functions or properties, class MUST be abstract
abstract class Foo1 {
    abstract fun foo()
    abstract val name: String
}

open class SomeFooSubclass1(override val name: String) : Foo1() {
    override fun foo() {
        TODO("Not yet implemented")
    }
}


interface Terminator {
    // all functions by default are abstract
    fun sayIllBeBack()
    fun sayAreYouSarahConnor()
}

class Ahnold: Terminator {
    override fun sayIllBeBack() {
        println("Ahh be bach")
    }
    override fun sayAreYouSarahConnor() {
        println("ah yew sa-aa conn-uh")
    }
}

class PeeWee: Terminator {
    override fun sayIllBeBack() {
        println("Ahhhhhhhh")
    }
    override fun sayAreYouSarahConnor() {
        println("Ahhhhhhhh")
    }
}

fun terminatorScript(
    terminator: Terminator
) {
    terminator.sayAreYouSarahConnor()
    terminator.sayIllBeBack()
}


open class SomeFooSubclass2: Foo1() {
    override val name: String = "ABC"

    override fun foo() {
        TODO("Not yet implemented")
    }
}


fun main() {
    // Person person = new Person()
    val person = Person()
    person.name = "Scott"
    person.age = 55
    println(person.name)
    println(person.age)

    println("====================================")
    val person2 = Person2("Scott", 55)
    println(person2.name)
    println(person2.age)

    println("====================================")
    val person3 = Person3("Mike", 30)
    println(person3.name)
    println(person3.age)
    println(person3.toString()) // toString() should be used for DEBUGGING ONLY

    println("====================================")
    val person4 = Person4("Mike", 30)
    println(person4) // toString() should be used for DEBUGGING ONLY

    println("====================================")
    val person5 = Person5("Mike", 30)
    println(person5) // toString() should be used for DEBUGGING ONLY

    val person5a = person5.copy(name = "Steve")
    println(person5a) // toString() should be used for DEBUGGING ONLY

    println(person5 === person5a)
//    val test1 = person5
//    println(person5 === test1)

    val (name5, age5) = person5
    println(name5)
    println(age5)

    val (n1, n2) = generateNextTwoNumbers(15)
    println(n1)
    println(n2)

    val (n1a, n2a) = generateNextTwoNumbers2(15)

    val string = "a:b:c"
    val (letter1, letter2, letter3) = string.split(':')
    println(letter1)
    println(letter2)
    println(letter3)

    terminatorScript(PeeWee())
    terminatorScript(Ahnold())
}



