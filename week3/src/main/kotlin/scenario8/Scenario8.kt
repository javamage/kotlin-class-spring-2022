package scenario8

// sealed interface limits subtypes
sealed interface Tool
class ScrewDriver: Tool
class Saw: Tool

open class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) { // A
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
    fun add(tool: ScrewDriver) { // B
        println("    Toolbox.add(ScrewDriver)")
        tools = tools + tool
    }
}

class SafeToolbox: Toolbox() {
    fun add(saw: Saw) {
        println("    SafeToolbox.add(Saw)")
        super.add(saw) // calls A
    }
}


fun main() {
    val safeToolbox = SafeToolbox()
    val toolsToAdd = listOf(Saw(), ScrewDriver())

    for(tool in toolsToAdd) {
        println("adding ${tool::class.simpleName} to safe toolbox")
        when(tool) {
            is Saw -> safeToolbox.add(tool)         // compile: add(Saw)
            is ScrewDriver -> safeToolbox.add(tool) // compile: add(ScrewDriver)
        }
    }
}
