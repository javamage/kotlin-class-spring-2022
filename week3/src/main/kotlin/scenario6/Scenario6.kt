package scenario6

// sealed interface limits subtypes
sealed interface Tool
class ScrewDriver: Tool
class Saw: Tool

class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) { // A
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
    fun add(tool: ScrewDriver) { // B
        println("    Toolbox.add(ScrewDriver)")
        tools = tools + tool
    }
}

fun main() {
    val toolbox: Toolbox = Toolbox()
    val toolsToAdd = listOf(Saw(), ScrewDriver())

    for(tool in toolsToAdd) {
        println("adding ${tool::class.simpleName} to toolbox")
        when(tool) {
            is Saw -> toolbox.add(tool) // resolves to add(Tool)
            is ScrewDriver -> toolbox.add(tool) // resolves to add(ScrewDriver)
        }
    }
}
