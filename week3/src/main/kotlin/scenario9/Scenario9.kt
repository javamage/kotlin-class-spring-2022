package scenario9

// sealed interface limits subtypes
sealed interface Tool
class ScrewDriver: Tool
class Saw: Tool

open class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) { // A
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
    fun add(tool: ScrewDriver) { // B
        println("    Toolbox.add(ScrewDriver)")
        tools = tools + tool
    }
    open fun add(tool: Saw) { // C
        println("    Toolbox.add(Saw)")
        tools = tools + tool
    }
}

class SafeToolbox: Toolbox() {
    override fun add(tool: Saw) {
        println("    SafeToolbox.add(Saw)")
        super.add(tool) // calls C
    }
}

fun main() {
    val safeToolbox: Toolbox = SafeToolbox()
    val toolsToAdd = listOf(Saw(), ScrewDriver())

    for(tool in toolsToAdd) {
        println("adding ${tool::class.simpleName} to safe toolbox")
        when(tool) {
            is Saw -> safeToolbox.add(tool)
                // compile: add(Saw) - resolved from Toolbox
                // runtime: SafeToolbox.add(Saw)
            is ScrewDriver -> safeToolbox.add(tool)
                // compile: add(ScrewDriver) - resolved from Toolbox
                // runtime: Toolbox.add(ScrewDriver)
        }
    }
}
