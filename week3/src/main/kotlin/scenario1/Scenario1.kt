package scenario1

interface Tool
class ScrewDriver: Tool
class Saw: Tool

class Toolbox {
    var tools = emptyList<Tool>()
        private set

    fun add(tool: Tool) {
        println("    Toolbox.add(Tool)")
        tools = tools + tool
    }
}

fun main() {
    val toolbox: Toolbox = Toolbox()
//    val saw: Tool = Saw()
//    val screwDriver: Tool = ScrewDriver()
    val saw = Saw()
    val screwDriver = ScrewDriver()

    println("adding saw to toolbox")
    toolbox.add(saw)
        // compile time - resolves to add(Tool)
        // runtime - which actual type is toolbox - look for add(Tool)
    println("adding screwdriver to toolbox")
    toolbox.add(screwDriver)
}
