# 605.603 Object-Oriented and Functional Programming in Kotlin

## Midterm Exam

March 10, 2022 730p-930p EST

Covers content of weeks 1-6

## Final Exam

May 5, 2022 730p-930p EST

Covers content of weeks 8-13

## Spring Break

No class on March 24

## Videos

   * Week 1: https://youtu.be/6BZu7bnTyv8
   * Week 2: https://youtu.be/tcwVrZNuL40
   * Week 3: https://youtu.be/y0v_-jsP_FI
   * Week 4: https://youtu.be/jYatU5KU1Rw
   * Week 5: https://youtu.be/r_8gpjdqkEo
   * Week 6: https://youtu.be/wiz2xhiqMNY
   * Week 7: (Midterm; no video)
   * Week 8: https://youtu.be/ogVrGInZXq4
   * Week 9: https://youtu.be/8_F0svoi0nA
   * Week 10: (instructor sick)
   * Week 11: https://youtu.be/_u0zRX3VGOU
   * Week 12: https://youtu.be/rBmzWAbvg6Q

## Assignments

Assignments are posted after class sessions. Assignments are one-week, and there will be 8-10 assignments (depending on how topics are presented)

## Topics

This is the general order of topics to be presented, but tends to change a bit while the term is progressing. Sometimes I'll go deeper into some topics, add new topics, and may delay or skip others.

   * Class Logistics and Introduction
   * Kotlin Playground
   * IntelliJ Setup
   * Gradle Basics
   * Variables, Values and Data Types
   * Expressions, Part I
   * Basic Functions
   * Statements
   * Labels
   * Higher-Order Functions
   * Lambdas
   * Intro to Object-Oriented Programming Concepts
   * Classes
   * Packages
   * Class Inheritance
   * Inner classes
   * Extension Functions
   * Nullability
   * Expressions, Part II
   * Scoping Functions
   * Common Collection Data Structures
   * Expressions, Part III
   * Functions on Data Structures
   * Interfaces
   * Objects
   * Class Delegation and Composition
   * Operator Overloading for use as Data Structures
   * Coding Assignments
   * Generics
   * Enumeration Types
   * Sealed Classes
   * Exceptions
   * File I/O
   * Late Variable Initialization
   * Property Delegation
   * Basic Functional Programming Concepts
   * Basic Functional Programming in Kotlin
   * Mixing Functional and Object-Oriented Programming
   * Builders and Domain-Specific Languages
   * Multitasking Concepts
   * Basic Threads and Executors
   * Coroutines
