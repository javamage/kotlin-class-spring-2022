package week1107

import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.FlowCollector
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.launch

class Sample {
    val flow1 = flow<Int> {
        emit(1)
        emit(2)
        emit(3)
        emit(4)
        emit(5)
        emit(6)
        emitStuff() // same as this.emitStuff()
    }

    suspend fun FlowCollector<Int>.emitStuff() {
        emit(10)
        emit(12)
        (30..50).forEach {
            emit(it)
        }
    }
    suspend fun MutableSharedFlow<Int>.emitStuff() {
        emit(10)
        emit(12)
        (30..50).forEach {
            emit(it)
        }
    }

    fun foo() {
        val list = listOf(1,2,3,4,5,6,7,8,9)
        val flow2 = list.asFlow()
        // generally we don't create a flow and collect it in the same function like this...
        MainScope().launch {
            flow2.collect {
                // do stuff with the flow
            }
        }

        val flow3 = flowOf(1,2,3,4,5,6,7,8)

        val flow4 = MutableSharedFlow<Int>()
        // expose as just a Flow<Int> to outsiders
        MainScope().launch {
            flow4.emitStuff()
        }


    }

}