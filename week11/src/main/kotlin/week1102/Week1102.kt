package week1102

import java.awt.FlowLayout
import javax.swing.JButton
import javax.swing.JFrame
import javax.swing.JLabel
import javax.swing.SwingUtilities
import javax.swing.WindowConstants
import kotlin.concurrent.thread

// preemptive multitasking
//    - CPU divides up the process power
// cooperative multitasking
//    - developer must be nice and code "yielding"
fun main() {
    JFrame().apply {
        layout = FlowLayout()
        val label = JLabel("")
        val button = JButton("Press Me").apply {
            addActionListener {
                // DON'T DO THIS - USE COROUTINES
//                val thread = object: Thread() {
//                    override fun run() {
//                        (1..100).forEach {
//                            SwingUtilities.invokeAndWait {
//                                label.text = it.toString() // swing allows UI property updates on other threads
//                            }
//                            sleep(50)
//                        }
//                    }
//                }
//                thread.start()
                thread {
                    (1..100).forEach {
                        SwingUtilities.invokeAndWait {
                            label.text = it.toString() // swing allows UI property updates on other threads
                        }
                        Thread.sleep(50)
                    }
                }

//                (1..100).forEach {
//                    label.text = it.toString()
//                    Thread.sleep(50)
//                }
            }
        }
        add(button)
        add(label)
        defaultCloseOperation = WindowConstants.EXIT_ON_CLOSE
        setSize(300, 200)
        isVisible = true
    }
}