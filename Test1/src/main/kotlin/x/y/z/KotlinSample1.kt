package com.javadude.x.y.z

import a.b.c.SunListener

interface Foo {
    fun foo()
}

val x = SunListener {
    println("x")
}

val y = object: SunListener {
    override fun sunRose() {
        println("y")
    }
    override fun sunSet() {
        println("y")
    }
}

fun interface Fee {
    fun fee()
}

fun doStuff(fee: Fee) {
    fee.fee()
}
fun doStuff2(fee: (String) -> Unit) {
    fee("s")
}

fun test() {
    doStuff { println("X") }
}