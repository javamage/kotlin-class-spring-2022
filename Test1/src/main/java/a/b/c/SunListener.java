package a.b.c;

public interface SunListener {
    void sunRose();

    default void sunSet() {
    }
}
