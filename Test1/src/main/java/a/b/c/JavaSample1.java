package a.b.c;

import com.javadude.x.y.z.Fee;
import com.javadude.x.y.z.Foo;
import com.javadude.x.y.z.KotlinSample1Kt;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;

public class JavaSample1 {
    public static void main(String[] args) {
        A a = new A();
        a.foo();

        KotlinSample1Kt.doStuff(() -> {

        });

        KotlinSample1Kt.doStuff2(s -> { return null; });
    }
}

class A implements Foo {
    @Override
    public void foo() {
        System.out.println("Hello");
    }
}


class B implements Fee {
    @Override
    public void fee() {
    }
}



