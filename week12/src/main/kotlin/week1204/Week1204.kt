package week1204

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

data class Counter1(var value: Int = 0) {
    fun increment() {
        val nextValue = value + 1
        value = nextValue
    }
}

fun main() {
    val context = CoroutineName("worker") + Dispatchers.Default
    val scope = CoroutineScope(context)

    val counter1 = Counter1()
    runBlocking {
        val deferred1 = scope.async {
            repeat(1000000) {
                counter1.increment()
            }
        }
        val deferred2 = scope.async {
            repeat(1000000) {
                counter1.increment()
            }
        }

        deferred1.await()
        deferred2.await()

        println(counter1.value)
    }
}