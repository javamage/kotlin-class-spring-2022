package week1203

import kotlin.concurrent.thread

fun main() {
    var x = 0
    x++ // looks atomic, but not
        // fetch x
        // increment x
        // write x

        // LOADINT 42876
        // INC
        // STOREINT 42876

    thread {
        repeat(10000) {
            x++
        }
    }
    thread {
        repeat(10000) {
            x++
        }
    }
}