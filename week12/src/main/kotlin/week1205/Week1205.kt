package week1205

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking
import java.util.concurrent.locks.ReentrantLock
import kotlin.concurrent.withLock

class MyLock

data class AtomicCounter1(var value: Int = 0) {
    private val valueChangeLock = MyLock()
    private val generalProcessingLock = MyLock()

    fun increment() {
        synchronized(valueChangeLock) {
            val nextValue = value + 1
            value = nextValue
        }
    }
    fun decrement() {
        synchronized(valueChangeLock) {
            val nextValue = value - 1
            value = nextValue
        }
    }
    fun doSomethingElse() {
        synchronized(generalProcessingLock) {
            // some stuff
        }
    }
}

data class AtomicCounter2(var value: Int = 0) {
    private val valueChangeLock = ReentrantLock()
    private val generalProcessingLock = ReentrantLock()

    fun increment() {
        valueChangeLock.withLock {
            val nextValue = value + 1
            value = nextValue
        }
    }
    fun decrement() {
        valueChangeLock.withLock {
            val nextValue = value - 1
            value = nextValue
        }
    }
    fun doSomethingElse() {
        generalProcessingLock.withLock {
            // some stuff
        }
    }
}

fun main() {
    val context = CoroutineName("worker") + Dispatchers.Default
    val scope = CoroutineScope(context)

    val counter1 = AtomicCounter1()
    runBlocking {
        val deferred1 = scope.async {
            repeat(1000000) {
                counter1.increment()
            }
        }
        val deferred2 = scope.async {
            repeat(1000000) {
                counter1.increment()
            }
        }

        deferred1.await()
        deferred2.await()

        println(counter1.value)
    }
}