package week1201

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

suspend fun foo() {
    delay(1000)
}

fun main() {
    val context = CoroutineName("worker") + Dispatchers.Default
    val scope = CoroutineScope(context)

    runBlocking {
        foo()
        val deferred1 = scope.async {
            repeat(10000) {
                println(it)
            }
            42
        }
        val deferred2 = scope.async {
            repeat(10000) {
                println(it)
            }
            10
        }

        // other code running concurrently as the coroutine

        val value1 = deferred1.await()
        val value2 = deferred2.await()
        println(value1 + value2)

//        scope.launch {
//            repeat(10000) {
//                println(it)
//            }
//        }
    }

    Thread.sleep(10)
}