package week1206

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.async
import kotlinx.coroutines.runBlocking

// deadlocks
// cowboy standoff
//    two shooters who are super honorable and refuse to shoot until the other does

// grab lock1, waiting for lock2
// someone else grabs lock2 and waits for lock1

class MyLock

class PossibleDeadlock {
    private val lockX = MyLock()
    private val lockY = MyLock()

    fun a() {
        synchronized(lockX) {
            synchronized(lockY) {
                Thread.sleep(100)
            }
        }
    }
    fun b() {
        synchronized(lockX) {
            synchronized(lockY) {
                Thread.sleep(33)
            }
        }
    }
}

fun main() {
    val context = CoroutineName("compute") + Dispatchers.Default
    val scope = CoroutineScope(context)

    val dl = PossibleDeadlock()

    runBlocking {
        val deferred1 = scope.async {
            repeat(100) {
                dl.a()
            }
            println("coroutine 1 finished")
        }
        val deferred2 = scope.async {
            repeat(100) {
                dl.b()
            }
            println("coroutine 2 finished")
        }

        deferred1.await()
        deferred2.await()
        println("all done")
    }
}