package week1202

import kotlinx.coroutines.CoroutineName
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.asCoroutineDispatcher
import kotlinx.coroutines.launch
import java.util.concurrent.Executors

fun main() {
    val executor = Executors.newFixedThreadPool(4)
    val dispatcher = executor.asCoroutineDispatcher()
    val context = CoroutineName("worker") + dispatcher
    val scope = CoroutineScope(context)

    scope.launch {
        repeat(10000) {
            println(it)
        }
        executor.shutdownNow()
    }

    // other stuff happening concurrently
}