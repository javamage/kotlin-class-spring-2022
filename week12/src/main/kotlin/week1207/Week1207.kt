package week1207

import lib1.myMutableListOf
import lib2.Funky

// NEVER USE IMPORT ON DEMAND
//import lib1.*
//import lib2.*

// APIS - Application Programming Interface

// libraries
//    some parts are hidden
//    some parts are public - APIs

// MOST IMPORTANT THING
//   Do everything you can to NOT change the API
//     OK
//        adding classes, interfaces, enums etc
//        adding new functions   - though might impact extension functions
//        adding new properties  - though might impact extension functions
//        adding new packages
//     BAD
//        adding/changing/reordering enum values
//           kinda similar for sealed classes/interfaces
//        deleting anything
//        renaming/reordering parameters or changing types
//        moving things between packages
//
// Think about abstract FAR MORE than the code behind the scenes would use abstraction
//    LOTS OF INTERFACES - one big part
//      - create concrete instances using factories

// consider SEMANTIC VERSIONING
//    vv.rr.mm.bbbb
//       vv - major version - if it changes, it's a BREAKING change
//       rr - revision - new functionality - almost always non-breaking
//       mm - mod - internal tweak - no API change
//       bbbb - build number

fun main() {
    val funky = Funky()

    val list = myMutableListOf("A", "B", "C")
    for (item in list) {
        println(item)
    }
}