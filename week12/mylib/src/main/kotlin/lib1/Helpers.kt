package lib1

fun <T> myMutableListOf(vararg items: T): MyMutableList<T> =
    MyMutableListImpl<T>().apply {
        items.forEach {
            add(it)
        }
    }