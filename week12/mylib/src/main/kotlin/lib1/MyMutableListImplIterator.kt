package lib1

internal class MyMutableListImplIterator<T>(head: ListNode<T>?): Iterator<T> {
    private var current = head
    override fun hasNext() = (current != null)
    override fun next() =
        current?.value?.apply {
            current = current?.next
        } ?: throw IllegalStateException()
}