package lib1

class MyMutableListImpl<T>: MyMutableList<T> {
    private var head: ListNode<T>? = null

    override fun iterator(): Iterator<T> = MyMutableListImplIterator(head)

    override fun add(item: T) {
        head = ListNode(item, head)
    }
    override fun remove(item: T) {
        var prev: ListNode<T>? = null
        var temp = head
        while (temp != null && temp.value != item) {
            prev = temp
            temp = temp.next
        }
        if (prev == null) {
            head = head?.next
        } else {
            prev.next = temp?.next
        }
    }
}