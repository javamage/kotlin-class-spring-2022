package lib1

internal data class ListNode<T>(
    val value: T,
    var next: ListNode<T>? = null
)