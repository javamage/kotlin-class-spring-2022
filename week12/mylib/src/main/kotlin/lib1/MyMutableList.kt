package lib1

interface MyMutableList<T>: Iterable<T> {
    fun add(item: T)
    fun remove(item: T)
}

